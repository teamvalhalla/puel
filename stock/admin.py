from django.contrib import admin

# Register your models here.
from .models import Ajuste, Coccion, Compra, ItemCompra, Ingrediente, ItemReceta, Receta

admin.site.site_header = "Inventario de Puel"
admin.site.site_title = "Portal de Puel"
admin.site.index_title = "Bienvenidos al portal de administración"

class ItemRecetaInLine(admin.TabularInline):
    model = ItemReceta
    extra = 4

class RecetaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['nombre']}),
    ]
    list_display = ('nombre', 'faltante')
    inlines = [ItemRecetaInLine]

class IngredienteAdmin(admin.ModelAdmin):
    fields = ['nombre']
    list_display = ('nombre', 'stock')


class ItemCompraInLine(admin.TabularInline):
    model = ItemCompra
    extra = 1


class CompraAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['fecha', 'detalle']}),
    ]
    inlines = [ItemCompraInLine]

admin.site.register(Receta, RecetaAdmin)
admin.site.register(Ingrediente, IngredienteAdmin)
admin.site.register(Compra, CompraAdmin)

admin.site.register(Ajuste),
admin.site.register(Coccion)

