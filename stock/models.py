from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
from bson.decimal128 import Decimal128

class Ingrediente(models.Model):
    nombre = models.CharField(max_length=150)
    stock = models.DecimalField(max_digits=6, decimal_places=2, default=0)

    def consumir(self, peso):
        stock_temp = self.getStock()
        if stock_temp - peso < 0:
            raise ValidationError(f"No existe stock suficiente de {self.__str__()}")
        else:
            stock_temp -= peso
            self.stock = stock_temp
            super().save()

    def reponer(self, peso):
        stock_temp = Decimal128(str(self.stock)).to_decimal()
        stock_temp = stock_temp + peso
        self.stock = stock_temp

    def getStock(self):
        return Decimal128(str(self.stock)).to_decimal()

    def __str__(self):
        return self.nombre


class ItemCompra(models.Model):
    ingrediente = models.ForeignKey(
        'Ingrediente',
        on_delete=models.RESTRICT,
    )
    compra = models.ForeignKey('Compra',
                                     on_delete=models.CASCADE)
    cantidad = models.IntegerField()
    peso = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    vencimiento = models.DateField()

    def __str__(self):
        return self.ingrediente.__str__() + ' por ' + self.peso.__str__() + ' kgs'

    def getPeso(self):
        return Decimal128(str(self.peso)).to_decimal()

    def save(self, *args, **kwargs):
        # Actualizar ingredientes

        self.ingrediente.reponer(self.peso * self.cantidad)
        self.ingrediente.save(*args, **kwargs)
        return super().save(*args, **kwargs)


class ItemReceta(models.Model):
    ingrediente = models.ForeignKey(
        'Ingrediente',
        on_delete=models.RESTRICT,
    )
    receta = models.ForeignKey(
        'Receta',
        on_delete=models.CASCADE,
    )
    peso = models.DecimalField(max_digits=6, decimal_places=2, default=0)

    def cocinar(self):
        self.ingrediente.consumir(self.getPeso())

    def getPeso(self):
        return Decimal128(str(self.peso)).to_decimal()

    def __str__(self):
        return self.ingrediente.__str__() + ' por ' + self.peso.__str__() + ' kgs'

    

class Receta(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre
        
    """
        Chequea si existen ingredientes suficientes para cocinar la receta
        @return list[Ingredientes] faltantes
    """
    def faltante(self):
        ingredientes_faltantes = [] #type: Ingrediente
        #se recorre la lista de ingredientes de la receta y se verifican que ingredientes faltan
        items = self.itemreceta_set.all()
        for item_receta in items:
            faltante = item_receta.getPeso() - item_receta.ingrediente.getStock()
            if faltante > 0:
                ingredientes_faltantes.append(f"{item_receta.ingrediente.__str__()} por {faltante} kgs")
        return ingredientes_faltantes


class Coccion(models.Model):
    receta = models.ForeignKey(
        'Receta',
        on_delete=models.RESTRICT,
    )
    fecha = models.DateField()

    def __str__(self):
        return self.receta.__str__() + ' el ' + self.fecha.__str__()

    def save(self, *args, **kwargs):
        #actualizar ingredientes
        item = []
        try:
            item = []
            items_receta = self.receta.itemreceta_set.all()
            for item_receta in items_receta:
                item_receta.cocinar()
                item.append(item_receta)
        except ValidationError:
            for item_receta in item:
                item_receta.ingrediente.reponer(item_receta.peso)
            raise
            
        return super().save(*args, **kwargs)


    class Meta:
        verbose_name = 'Coccion'
        verbose_name_plural = 'Cocciones'


class Compra(models.Model):
    fecha = models.DateField()
    detalle = models.CharField(max_length=300)

    def __str__(self):
        return self.fecha.__str__()


class Ajuste(models.Model):
    ingredientes = models.ForeignKey('Ingrediente',
        on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=6, decimal_places=2, default=0)

    def __str__(self):
        return self.ingredientes.__str__() + ' por ' + self.cantidad.__str__() + ' kgs'

    def getCantidad(self):
        return Decimal128(str(self.cantidad)).to_decimal()

    def save(self, *args, **kwargs):
        # actualizar ingredientes
        try:
            self.ingredientes.consumir(self.cantidad)
        except ValidationError as e:
            print(e)
            raise
        return super().save(*args, **kwargs)
